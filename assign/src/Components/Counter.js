import React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { increment,decrement } from '../actions';
const Counter = () => {
const count = useSelector((state) => state.counter.count);
const dispatch = useDispatch();


return (
<div>
<h1>Books prsentNow: {count}</h1>
<button onClick={() => dispatch(increment())}>Return</button>
<button onClick={() => dispatch(decrement())}>Buy</button>
</div>
);
};


export default Counter;