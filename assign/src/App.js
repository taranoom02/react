import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Counter from './Components/Counter';
import './App.css'
function App() {
return (
<Provider store={store}>
<div className="App">
<nav className="navbar">
          <div className="navbar-content">
            <span className="navbar-title">Bookstore</span>
          </div>
        </nav>
    <h1>Total books:100</h1>    
<Counter />
</div>
</Provider>
);
}


export default App;
